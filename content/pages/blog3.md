---
title: "CronControl"
description: "Scaling your offload jobs - asynchronously"
slug: "cron"
image: pic99.jpg
keywords: ""
categories: 
    - ""
    - ""
date: 2021-10-31T22:26:13-05:00
draft: false
---

## Frustated with stuck queues ?

There is no reason to use external cron triggers or strange triggered-by-visitors hooks.

All recurring jobs can be optimized, parellelized 
and finally randomized in start time so your infastructure is not on overload 
