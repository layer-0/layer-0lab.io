---
title: "Integrating"
description: "Legacy or single-threaded stuff comes to life again "
slug: "integrate"
image: pic10.jpg
keywords: ""
categories: 
    - ""
    - ""
date: 2021-10-31T21:28:43-05:00
draft: false
---


Typical Scenarios:

* legacy things are put into redundant environments where they can safely reside

* After a transition time a refactored version will be put in production

* legacy applications in virtual environments are possible als last resort
